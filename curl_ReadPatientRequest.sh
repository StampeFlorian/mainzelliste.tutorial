#!/bin/bash

tokenId=$(awk -F'"' '{ print $4 }' ReadPatientRequest);
baseurl=http://localhost:8080/patients/tokenId/
tempurl=${baseurl}${tokenId}
baseurl=$(echo "$baseurl" | sed 's: ::g')
tokenId=$(echo "$tokenId" | sed 's: ::g')
tempurl=$(echo "$baseurl" | echo "$tokenId"| sed 's: ::g')
url=$(echo "$tempurl" | sed 's: ::g')
echo ${url}

curl -v -X GET \
  "${url}"\
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'mainzellisteApiKey: changeThisApiKey' \
  -H 'mainzellisteApiVersion: 2.0'