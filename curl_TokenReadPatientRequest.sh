#!/bin/bash

sessionId=$(awk -F'"' '{ print $4 }' SessionRequestAnswer);
idString=$(awk -F'"' '{ print $4 }' AddPatientRequest);

echo ${idString};
echo ${sessionId};

curl -v -i -X POST \
  http://localhost:8080/sessions/"${sessionId}"/tokens \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'mainzellisteApiKey: changeThisApiKey' \
  -H 'mainzellisteApiVersion: 2.0' \
  -d '{
	"type": "readPatients",
 	"data": {
	 	"searchIds": [
	 		{
	 			"idType":"pid",
	 			"idString":"'"${idString}"'"
	 		}
		],
		"resultFields": ["vorname", "nachname"]
 	}
}' \
> ./ReadPatientRequest