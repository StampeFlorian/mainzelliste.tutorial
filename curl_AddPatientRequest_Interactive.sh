#!/bin/bash

function generatePostData {
	cat<<EOF
vorname=$vorname&nachname=$nachname&geburtsname=$nachname&geburtstag=$geburtstag&geburtsmonat=$geburtsmonat&geburtsjahr=$geburtsjahr&ort=$ort&plz=$plz
EOF
}

function sendHttpRequest (){
    curl -v --header "apiKey: changeThisApiKey" \
            --request POST \
            --data "$(generatePostData)" \
            "$urlbase"/patients?tokenId="${tokenId}"
}

#Start

tokenId=$(awk -F'"' '{ print $4 }' ./TokenAddPatientRequest);

echo "${tokenId}"

read -p 'vorname: ' vorname
vorname=${vorname:-max}
read -p 'nachname: ' nachname
nachname=${nachname:-test}
read -p 'geburtsjahr: ' geburtsjahr
geburtsjahr=${geburtsjahr:-2000}
read -p 'geburtsmonat: ' geburtsmonat
geburtsmonat=${geburtsmonat:-01}
read -p 'geburtstag: ' geburtstag
geburtstag=${geburtstag:-01}
read -p 'ort: ' ort
ort=${ort:-Musterstadt}
read -p 'plz: ' plz
plz=${plz:-68309}
read -p 'urlbase: ' urlbase
urlbase=${urlbase:-http://localhost:8080}

sendHttpRequest
