# Repository für Mainzelliste Tutorial 12.11.2019

## Mainzelliste über Docker ausführen

### Docker-Compose für Mainzelliste RC 1.8 herunterladen

```curl -O https://bitbucket.org/medicalinformatics/mainzelliste/raw/7a00e5e90fb5f94a397f74c9930bc83345659370/docker-compose.yml```

### Mainzelliste und Postgres-Datenbank starten

docker-compose up

### Überprüfen ob die Mainzelliste hochgefahren ist

docker ps

Mit dem Browser localhost:8080 aufrufen

### Kommunizieren mit der Mainzelliste

#### Session anlegen

Mit wget: 
```wget  --method POST   --header 'mainzellisteApiKey: changeThisApiKey'   --header 'cache-control: no-cache'  --body-data '{\n}\n'   --output-document  - http://localhost:8080/sessions > ./SessionRequestAnswer && cat SessionRequestAnswer```

Mit cURL:
```curl -X POST http://localhost:8080/sessions -H "mainzellisteApiKey: changeThisApiKey" -H 'mainzellisteApiVersion: 2.0' > SessionRequestAnswer && cat SessionRequestAnswer```

#### Token anlegen (AddPatient)

SessionId aus SessionRequestAnswer lesen:  ```awk -F'"' '{ print $4 }' SessionRequestAnswer ```

SessionId auslesen und AddPatientToken anlegen (curl_TokenAddPatientRequest-sh)


```bash
sessionId=$(awk -F'"' '{ print $4 }' SessionRequestAnswer);

curl POST http://localhost:8080/sessions/"${sessionId}"/tokens -H 'Content-Type: application/json' -H 'cache-control: no-cache'   -H 'mainzellisteApiKey: changeThisApiKey'   -H 'mainzellisteApiVersion: 2.0'   -d '{
"type": "addPatient",
     "data": {
     "idtypes": [pid]
    }
}' > TokenAddPatientRequest && cat TokenAddPatientRequest
```

#### Patienten hinzufügen
```./curl_AddPatientRequest.sh```

#### Session + Token (AddPatients) + Patienten anlegen 
```./curl_SessionRequest.sh && ./curl_TokenAddPatientRequest.sh && ./curl_AddPatientRequest.sh```


#### Interaktiv: Session + Token (AddPatients) + Patienten anlegen 
```./curl_SessionRequest.sh && ./curl_TokenAddPatientRequest.sh && ./curl_AddPatientRequest_Interactive.sh```


#### Session + Token (AddPatients) + Patienten anlegen + Token (ReadPatients) + Patient lesen
```./curl_SessionRequest.sh && ./curl_TokenAddPatientRequest.sh && ./curl_AddPatientRequest.sh && ./curl_TokenReadPatientRequest.sh && ./curl_ReadPatientRequest.sh```

#### Docker
Information über das offizielle Docker Image sind [im offiziellen Repository zu finden](https://bitbucket.org/medicalinformatics/mainzelliste/src/7a00e5e90fb5f94a397f74c9930bc83345659370/docker.md)