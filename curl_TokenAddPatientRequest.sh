#!/bin/sh
sessionId=$(awk -F'"' '{ print $4 }' SessionRequestAnswer);

curl POST http://localhost:8080/sessions/"${sessionId}"/tokens -H 'Content-Type: application/json' -H 'cache-control: no-cache'   -H 'mainzellisteApiKey: changeThisApiKey'   -H 'mainzellisteApiVersion: 2.0'   -d '{
"type": "addPatient",
     "data": {
     "idtypes": [pid]
    }
}' > TokenAddPatientRequest && cat TokenAddPatientRequest