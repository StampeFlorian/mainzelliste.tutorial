#!/bin/bash

function getPostData {
	cat<<EOF
vorname=vorname&nachname=nachname&geburtsname=nachname&geburtstag=11&geburtsmonat=04&geburtsjahr=1982&ort=Mannheim&plz=68309
EOF
}

function sendHttpRequest (){
    curl -v --header "apiKey: changeThisApiKey" \
            --request POST \
            --data "$(getPostData)" \
            "$urlbase"/patients?tokenId="${tokenId}" > ./AddPatientRequest
}

#Start

tokenId=$(awk -F'"' '{ print $4 }' ./TokenAddPatientRequest);
urlbase=${urlbase:-http://localhost:8080}

sendHttpRequest
